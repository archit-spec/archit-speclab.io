#!/bin/bash

# Function to print colorful text
print_colorful() {
  # $1: The text to be printed
  # $2: The ANSI color code
  echo -e "${2}${1}\e[0m"
}

# Function to build the zola project
build_zola() {
  zola build
  print_colorful "zola built" "\e[32m"  # Green color for success message
}

build_zola

# Function to rsync the public directory and show a colorful message with ASCII art
rsync_public() {
  rsync -avL --progress -e "ssh -i ~/.ssh/dumbla.pem" \
    -r ~/myblog/public ubuntu@ec2-3-110-148-130.ap-south-1.compute.amazonaws.com:/var/www/html

  # Multi-line string with ASCII art and color codes
  print_colorful "
  ________
  Site updated!
  _________
  " "\e[36m"  # Cyan color for the ASCII art message
}

rsync_public
